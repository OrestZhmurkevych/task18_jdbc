package edu.jdbc.dao;

import edu.jdbc.model.CarEntity;

public interface CarsDAO extends GeneralDAO<CarEntity, String> {
}
