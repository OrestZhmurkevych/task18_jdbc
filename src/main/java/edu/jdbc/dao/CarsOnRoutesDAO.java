package edu.jdbc.dao;

import edu.jdbc.model.CarsOnRoutesEntity;

import java.util.List;

public interface CarsOnRoutesDAO {

    List<CarsOnRoutesEntity> findAll() throws Exception;

    List<String> findCarsByRouteId(String routeId) throws Exception;

    boolean create(CarsOnRoutesEntity entity) throws Exception;

    boolean addCarsToReservedRoute(List<String> carIds) throws Exception;

    boolean update(CarsOnRoutesEntity entity, String oldCarId, String oldRouteId) throws Exception;

    boolean delete(String carId, String routeId) throws Exception;
}
