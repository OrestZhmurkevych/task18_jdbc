package edu.jdbc.dao;

import edu.jdbc.model.ParametersOfUsageEntity;

public interface ParametersOfUsageDAO extends GeneralDAO<ParametersOfUsageEntity, String> {
}
