package edu.jdbc.dao;

import edu.jdbc.model.RouteEntity;

public interface RoutesDAO extends GeneralDAO<RouteEntity, String> {
}
