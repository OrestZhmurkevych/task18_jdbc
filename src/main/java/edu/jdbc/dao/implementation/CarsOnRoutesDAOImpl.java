package edu.jdbc.dao.implementation;

import edu.jdbc.dao.CarsOnRoutesDAO;
import edu.jdbc.model.CarsOnRoutesEntity;
import edu.jdbc.utils.ConnectionUtil;
import edu.jdbc.utils.DriverUtil;
import edu.jdbc.utils.Transformer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static edu.jdbc.common.Constants.*;
import static edu.jdbc.common.Queries.*;


public class CarsOnRoutesDAOImpl implements CarsOnRoutesDAO {

    private static Logger logger = LogManager.getLogger("CarsOnRoutesDAOImpl");

    @Override
    public List<CarsOnRoutesEntity> findAll() throws SQLException {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        List<CarsOnRoutesEntity> carsOnRoutes = new ArrayList<>();
        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(FIND_ALL_CARS_ON_ROUTES)) {
            while (rs.next()) {
                carsOnRoutes.add((CarsOnRoutesEntity) new Transformer(CarsOnRoutesEntity.class).fromResultSetToEntity(rs));
            }
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return carsOnRoutes;
    }

    @Override
    public List<String> findCarsByRouteId(String routeId) throws SQLException {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        List<String> carIds = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(FIND_CARS_BY_ROUTE_ID)) {
            statement.setString(ONE, routeId);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                carIds.add(rs.getString("car_id"));
            }
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return carIds;
    }

    @Override
    public boolean create(CarsOnRoutesEntity entity) throws SQLException {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        int numberOfUpdatedRows = ZERO;
        try (PreparedStatement statement = connection.prepareStatement(CREATE_CAR_ON_ROUTE)) {
            statement.setString(ONE, String.valueOf(entity.getCarId()));
            statement.setString(TWO, String.valueOf(entity.getRouteId()));
            numberOfUpdatedRows = statement.executeUpdate();
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return numberOfUpdatedRows > ZERO;
    }

    @Override
    public boolean addCarsToReservedRoute(List<String> carIds) throws SQLException {
        Integer reservedRouteId = FOUR;
        boolean result = false;
        for (String carId : carIds) {
            CarsOnRoutesEntity entity = new CarsOnRoutesEntity(Integer.valueOf(carId), reservedRouteId);
            result = create(entity);
            if (!result) {
                break;
            }
        }
        return result;
    }

    @Override
    public boolean update(CarsOnRoutesEntity entity, String oldCarId, String oldRouteId) throws SQLException {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        int numberOfUpdatedRows = ZERO;
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_CARS_ON_ROUTES)) {
            statement.setString(ONE, String.valueOf(entity.getCarId()));
            statement.setString(TWO, String.valueOf(entity.getRouteId()));
            statement.setString(THREE, oldCarId);
            statement.setString(FOUR, oldRouteId);
            numberOfUpdatedRows = statement.executeUpdate();
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return numberOfUpdatedRows > ZERO;
    }

    @Override
    public boolean delete(String carId, String routeId) throws SQLException {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        int numberOfUpdatedRows = ZERO;
        try (PreparedStatement statement = connection.prepareStatement(DELETE_CAR_ON_ROUTE)) {
            statement.setString(ONE, carId);
            statement.setString(TWO, routeId);
            numberOfUpdatedRows = statement.executeUpdate();
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return numberOfUpdatedRows > ZERO;
    }
}
