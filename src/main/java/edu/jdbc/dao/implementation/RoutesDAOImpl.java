package edu.jdbc.dao.implementation;

import edu.jdbc.dao.CarsOnRoutesDAO;
import edu.jdbc.dao.RoutesDAO;
import edu.jdbc.model.RouteEntity;
import edu.jdbc.utils.ConnectionUtil;
import edu.jdbc.utils.DriverUtil;
import edu.jdbc.utils.Transformer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static edu.jdbc.common.Constants.*;
import static edu.jdbc.common.Queries.*;

public class RoutesDAOImpl implements RoutesDAO {

    private static Logger logger = LogManager.getLogger("RoutesDAOImpl");
    private CarsOnRoutesDAO carsOnRoutesDAO = new CarsOnRoutesDAOImpl();

    public List<RouteEntity> findAll() throws SQLException {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        List<RouteEntity> routes = new ArrayList<>();
        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(FIND_ALL_ROUTES)) {
            while (rs.next()) {
                routes.add((RouteEntity) new Transformer(RouteEntity.class).fromResultSetToEntity(rs));
            }
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
        }
        return routes;
    }

    @Override
    public RouteEntity findByID(String id) throws SQLException {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        RouteEntity entity = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_ROUTE_BY_ID)) {
            statement.setString(ONE, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                entity = (RouteEntity) new Transformer(RouteEntity.class).fromResultSetToEntity(rs);
            }
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return entity;
    }

    @Override
    public boolean create(RouteEntity entity) throws SQLException {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        int numberOfUpdatedRows = ZERO;
        try (PreparedStatement statement = connection.prepareStatement(CREATE_ROUTE)) {
            statement.setString(ONE, String.valueOf(entity.getId()));
            statement.setString(TWO, entity.getEndPoints());
            statement.setString(THREE, String.valueOf(entity.getApproximateRoadTime()));
            statement.setString(FOUR, entity.getComplexity());
            numberOfUpdatedRows = statement.executeUpdate();
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return numberOfUpdatedRows > ZERO;
    }

    @Override
    public boolean update(RouteEntity entity) throws SQLException {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        int numberOfUpdatedRows = ZERO;
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_ROUTE)) {
            statement.setString(ONE, entity.getEndPoints());
            statement.setString(TWO, String.valueOf(entity.getApproximateRoadTime()));
            statement.setString(THREE, entity.getComplexity());
            statement.setString(FOUR, String.valueOf(entity.getId()));
            numberOfUpdatedRows = statement.executeUpdate();
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return numberOfUpdatedRows > ZERO;
    }

    @Override
    public boolean delete(String id) throws Exception {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        final List<String> carsByRouteId = carsOnRoutesDAO.findCarsByRouteId(id);
        carsOnRoutesDAO.addCarsToReservedRoute(carsByRouteId);
        for (String carId : carsByRouteId) {
            carsOnRoutesDAO.delete(carId, id);
        }
        int numberOfUpdatedRows = ZERO;
        try (PreparedStatement statement = connection.prepareStatement(DELETE_ROUTE)) {
            statement.setString(ONE, id);
            numberOfUpdatedRows = statement.executeUpdate();
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return numberOfUpdatedRows > ZERO;
    }
}
