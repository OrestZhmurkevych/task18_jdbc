package edu.jdbc.dao.implementation;

import edu.jdbc.dao.ParametersOfUsageDAO;
import edu.jdbc.model.ParametersOfUsageEntity;
import edu.jdbc.utils.ConnectionUtil;
import edu.jdbc.utils.DriverUtil;
import edu.jdbc.utils.Transformer;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static edu.jdbc.common.Constants.*;
import static edu.jdbc.common.Queries.*;


public class ParametersOfUsageDAOImpl implements ParametersOfUsageDAO {

    private static Logger logger = LogManager.getLogger("ParametersOfUsageDAOImpl");

    @Override
    public List<ParametersOfUsageEntity> findAll() throws Exception {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        List<ParametersOfUsageEntity> parametersOfUsageEntities = new ArrayList<>();
        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(FIND_ALL_PARAMETERS_OF_USAGE)) {
            while (rs.next()) {
                parametersOfUsageEntities.add(
                        (ParametersOfUsageEntity) new Transformer(ParametersOfUsageEntity.class).fromResultSetToEntity(rs));
            }
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return parametersOfUsageEntities;
    }

    @Override
    public ParametersOfUsageEntity findByID(String id) throws Exception {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        ParametersOfUsageEntity entity = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_PARAMETERS_OF_USAGE_BY_ID)) {
            statement.setString(ONE, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                entity = (ParametersOfUsageEntity) new Transformer(ParametersOfUsageEntity.class).fromResultSetToEntity(rs);
            }
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return entity;
    }

    @Override
    public boolean create(ParametersOfUsageEntity entity) throws Exception {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        int numberOfUpdatedRows = ZERO;
        try (PreparedStatement statement = connection.prepareStatement(CREATE_PARAMETERS_OF_USAGE)) {
            statement.setString(ONE, String.valueOf(entity.getId()));
            statement.setString(TWO, String.valueOf(entity.getOverallMileage()));
            statement.setString(THREE, String.valueOf(entity.getCitiesVisited()));
            statement.setString(FOUR, entity.getOverallCondition());
            numberOfUpdatedRows = statement.executeUpdate();
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return numberOfUpdatedRows > ZERO;
    }

    @Override
    public boolean update(ParametersOfUsageEntity entity) throws Exception {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        int numberOfUpdatedRows = ZERO;
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_PARAMETERS_OF_USAGE)) {
            statement.setString(ONE, String.valueOf(entity.getOverallMileage()));
            statement.setString(TWO, String.valueOf(entity.getCitiesVisited()));
            statement.setString(THREE, entity.getOverallCondition());
            statement.setString(FOUR, String.valueOf(entity.getId()));
            numberOfUpdatedRows = statement.executeUpdate();
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return numberOfUpdatedRows > ZERO;
    }

    @Override
    public boolean delete(String id) throws Exception {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        int numberOfUpdatedRows = ZERO;
        try (PreparedStatement statement = connection.prepareStatement(DELETE_PARAMETERS_OF_USAGE)) {
            statement.setString(ONE, String.valueOf(id));
            numberOfUpdatedRows = statement.executeUpdate();
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return numberOfUpdatedRows > ZERO;
    }
}
