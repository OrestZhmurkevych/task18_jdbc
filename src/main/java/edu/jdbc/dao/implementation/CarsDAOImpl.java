package edu.jdbc.dao.implementation;

import edu.jdbc.dao.CarsDAO;
import edu.jdbc.model.CarEntity;
import edu.jdbc.utils.Transformer;
import edu.jdbc.utils.ConnectionUtil;
import edu.jdbc.utils.DriverUtil;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static edu.jdbc.common.Constants.*;
import static edu.jdbc.common.Queries.*;


public class CarsDAOImpl implements CarsDAO {

    private static Logger logger = LogManager.getLogger("CarsDAOImpl");

    public List<CarEntity> findAll() throws SQLException {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        List<CarEntity> cars = new ArrayList<>();
        try (Statement statement = connection.createStatement();
             ResultSet rs = statement.executeQuery(FIND_ALL_CARS)) {
            while (rs.next()) {
                cars.add((CarEntity) new Transformer(CarEntity.class).fromResultSetToEntity(rs));
            }
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return cars;
    }

    @Override
    public CarEntity findByID(String id) throws SQLException {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        CarEntity entity = null;
        try (PreparedStatement statement = connection.prepareStatement(FIND_CAR_BY_ID)) {
            statement.setString(ONE, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                entity = (CarEntity) new Transformer(CarEntity.class).fromResultSetToEntity(rs);
            }
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return entity;
    }

    @Override
    public boolean create(CarEntity entity) throws SQLException {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        int numberOfUpdatedRows = ZERO;
        try (PreparedStatement statement = connection.prepareStatement(CREATE_CAR)) {
            statement.setString(ONE, String.valueOf(entity.getId()));
            statement.setString(TWO, entity.getBrand());
            statement.setString(THREE, entity.getModel());
            statement.setString(FOUR, entity.getType());
            statement.setString(FIVE, String.valueOf(entity.getMaxSpeed()));
            statement.setString(SIX, String.valueOf(entity.getParametersOfUsageId()));
            numberOfUpdatedRows = statement.executeUpdate();
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return numberOfUpdatedRows > ZERO;
    }

    @Override
    public boolean update(CarEntity entity) throws SQLException {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        int numberOfUpdatedRows = ZERO;
        try (PreparedStatement statement = connection.prepareStatement(UPDATE_CAR)) {
            statement.setString(ONE, entity.getBrand());
            statement.setString(TWO, entity.getModel());
            statement.setString(THREE, entity.getType());
            statement.setString(FOUR, String.valueOf(entity.getMaxSpeed()));
            statement.setString(FIVE, String.valueOf(entity.getParametersOfUsageId()));
            statement.setString(SIX, String.valueOf(entity.getId()));
            numberOfUpdatedRows = statement.executeUpdate();
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return numberOfUpdatedRows > ZERO;
    }

    @Override
    public boolean delete(String id) throws SQLException {
        DriverUtil.getDriver();
        Connection connection = ConnectionUtil.getConnection();
        int numberOfUpdatedRows = ZERO;
        try (PreparedStatement statement = connection.prepareStatement(DELETE_CAR)) {
            statement.setString(ONE, String.valueOf(id));
            numberOfUpdatedRows = statement.executeUpdate();
        } catch (SQLException e) {
            logger.info("SQL exception: " + e.getMessage());
            throw e;
        }
        return numberOfUpdatedRows > ZERO;
    }
}
