package edu.jdbc.model;

import edu.jdbc.model.annotations.Column;
import edu.jdbc.model.annotations.PrimaryKey;
import edu.jdbc.model.annotations.Table;


@Table(name = "parameters_of_usage")
public class ParametersOfUsageEntity {

    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "overall_mileage")
    private Integer overallMileage;
    @Column(name = "cities_visited")
    private Integer citiesVisited;
    @Column(name = "overall_condition", length = 45)
    private String overallCondition;

    public ParametersOfUsageEntity() {
    }

    public ParametersOfUsageEntity(Integer id, Integer overallMileage, Integer citiesVisited, String overallCondition) {
        this.id = id;
        this.overallMileage = overallMileage;
        this.citiesVisited = citiesVisited;
        this.overallCondition = overallCondition;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOverallMileage() {
        return overallMileage;
    }

    public void setOverallMileage(Integer overallMileage) {
        this.overallMileage = overallMileage;
    }

    public Integer getCitiesVisited() {
        return citiesVisited;
    }

    public void setCitiesVisited(Integer citiesVisited) {
        this.citiesVisited = citiesVisited;
    }

    public String getOverallCondition() {
        return overallCondition;
    }

    public void setOverallCondition(String overallCondition) {
        this.overallCondition = overallCondition;
    }

    @Override
    public String toString() {
        return String.format("%-1s %-1s %-1s %-1s ", id, overallMileage, citiesVisited, overallCondition);
    }
}
