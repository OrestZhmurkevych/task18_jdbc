package edu.jdbc.model;

import edu.jdbc.model.annotations.Column;
import edu.jdbc.model.annotations.PrimaryKey;
import edu.jdbc.model.annotations.Table;


@Table(name = "cars")
public class CarEntity {

    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "brand", length = 45)
    private String brand;
    @Column(name = "model", length = 45)
    private String model;
    @Column(name = "type", length = 45)
    private String type;
    @Column(name = "max_speed")
    private Integer maxSpeed;
    @Column(name = "parameters_of_usage_id")
    private Integer parametersOfUsageId;

    public CarEntity() {
    }

    public CarEntity(Integer id, String brand, String model, String type, Integer maxSpeed, Integer parametersOfUsageId) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.type = type;
        this.maxSpeed = maxSpeed;
        this.parametersOfUsageId = parametersOfUsageId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(Integer maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public Integer getParametersOfUsageId() {
        return parametersOfUsageId;
    }

    public void setParametersOfUsageId(Integer parametersOfUsageId) {
        this.parametersOfUsageId = parametersOfUsageId;
    }

    @Override
    public String toString() {
        return String.format("%-1s %-1s %-1s %-1s %-1s %-1s ",
                id, brand, model, type, maxSpeed, parametersOfUsageId);
    }
}
