package edu.jdbc.model;

import edu.jdbc.model.annotations.Column;
import edu.jdbc.model.annotations.Table;


@Table(name = "cars_on_routes")
public class CarsOnRoutesEntity {

    @Column(name = "car_id")
    private Integer carId;
    @Column(name = "route_id")
    private Integer routeId;

    public CarsOnRoutesEntity() {
    }

    public CarsOnRoutesEntity(Integer carId, Integer routeId) {
        this.carId = carId;
        this.routeId = routeId;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    @Override
    public String toString() {
        return String.format("%-1s %-1s ", carId, routeId);
    }
}
