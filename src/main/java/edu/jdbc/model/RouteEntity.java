package edu.jdbc.model;

import edu.jdbc.model.annotations.Column;
import edu.jdbc.model.annotations.PrimaryKey;
import edu.jdbc.model.annotations.Table;


@Table(name = "routes")
public class RouteEntity {

    @PrimaryKey
    @Column(name = "id")
    private Integer id;
    @Column(name = "end_points", length = 45)
    private String endPoints;
    @Column(name = "approximate_road_time")
    private Integer approximateRoadTime;
    @Column(name = "complexity",length = 45)
    private String complexity;

    public RouteEntity() {
    }

    public RouteEntity(int id, String endPoints, int approximateRoadTime, String complexity) {
        this.id = id;
        this.endPoints = endPoints;
        this.approximateRoadTime = approximateRoadTime;
        this.complexity = complexity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEndPoints() {
        return endPoints;
    }

    public void setEndPoints(String endPoints) {
        this.endPoints = endPoints;
    }

    public int getApproximateRoadTime() {
        return approximateRoadTime;
    }

    public void setApproximateRoadTime(int approximateRoadTime) {
        this.approximateRoadTime = approximateRoadTime;
    }

    public String getComplexity() {
        return complexity;
    }

    public void setComplexity(String complexity) {
        this.complexity = complexity;
    }

    @Override
    public String toString() {
        return String.format("%-1s %-1s %-1s %-1s ", id, endPoints, approximateRoadTime, complexity);
    }
}
