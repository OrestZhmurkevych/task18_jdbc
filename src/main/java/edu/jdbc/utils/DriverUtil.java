package edu.jdbc.utils;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class DriverUtil {

    private static Logger logger = LogManager.getLogger("App");

    public static void getDriver() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            logger.info("The driver for MySQL wasn't loaded");
        }
    }
}
