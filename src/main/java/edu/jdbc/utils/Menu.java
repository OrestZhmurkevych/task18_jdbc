package edu.jdbc.utils;

import edu.jdbc.dao.CarsDAO;
import edu.jdbc.dao.CarsOnRoutesDAO;
import edu.jdbc.dao.ParametersOfUsageDAO;
import edu.jdbc.dao.RoutesDAO;
import edu.jdbc.dao.implementation.CarsDAOImpl;
import edu.jdbc.dao.implementation.CarsOnRoutesDAOImpl;
import edu.jdbc.dao.implementation.ParametersOfUsageDAOImpl;
import edu.jdbc.dao.implementation.RoutesDAOImpl;
import edu.jdbc.model.CarEntity;
import edu.jdbc.model.CarsOnRoutesEntity;
import edu.jdbc.model.ParametersOfUsageEntity;
import edu.jdbc.model.RouteEntity;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Menu {

    private static Logger logger = LogManager.getLogger("Menu");
    private static Scanner scanner = new Scanner(System.in);

    public void show() throws Exception {

        logger.info("Choose the action");
        logger.info("Select all table - press 1");
        logger.info("Select the table to interact - press 2");
        int choice = scanner.nextInt();
        switch (choice) {
            case 1:
                selectAll();
                break;
            case 2:
                selectTable();
                break;
        }
    }

    public void selectAll() throws Exception {
        CarsDAO carsDAO = new CarsDAOImpl();
        ParametersOfUsageDAO parametersOfUsage = new ParametersOfUsageDAOImpl();
        CarsOnRoutesDAO carsOnRoutesDAO = new CarsOnRoutesDAOImpl();
        RoutesDAOImpl routesDAO = new RoutesDAOImpl();
        logger.info(carsDAO.findAll());
        logger.info(parametersOfUsage.findAll());
        logger.info(carsOnRoutesDAO.findAll());
        logger.info(routesDAO.findAll());
    }

    public void selectTable() throws Exception {
        logger.info("Select the table");
        logger.info("Table cars - press 1");
        logger.info("Table routes - press 2");
        logger.info("Table parameters_of_usage - press 3");
        logger.info("Table cars_on_routes - press 4");
        int choice = scanner.nextInt();
        switch (choice) {
            case 1:
                selectTableCarsAction();
                break;
            case 2:
                selectTableRoutesAction();
                break;
            case 3:
                selectTableParametersOfUsageAction();
                break;
            case 4:
                selectTableCarsOnRoutesAction();
                break;
        }
    }

    public void selectTableCarsAction() throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        CarsDAO carsDAO = new CarsDAOImpl();
        logger.info("Table cars");
        logger.info("Select all - press 1");
        logger.info("Select cars by id - press 2");
        logger.info("Create car - press 3");
        logger.info("Update car - press 4");
        logger.info("Delete car by id - press 5");
        int choice = Integer.valueOf(br.readLine());
        switch (choice) {
            case 1:
                logger.info(carsDAO.findAll());
                break;
            case 2:
                logger.info("Enter id for search");
                int searchId = Integer.valueOf(br.readLine());
                logger.info(carsDAO.findByID(String.valueOf(searchId)));
                break;
            case 3:
                logger.info("Enter the parameters for car you want to create");
                logger.info("Id:");
                Integer createId = Integer.valueOf(br.readLine());
                logger.info("Brand:");
                String createBrand = br.readLine();
                logger.info("Model:");
                String createModel = br.readLine();
                logger.info("Type:");
                String createType = br.readLine();
                logger.info("Max speed:");
                Integer createMaxSpeed = Integer.valueOf(br.readLine());
                logger.info("Parameters of usage:");
                Integer createParametersOfUsage = Integer.valueOf(br.readLine());
                CarEntity carCreateEntity = new CarEntity(createId, createBrand, createModel,
                        createType, createMaxSpeed, createParametersOfUsage);
                final boolean createResult = carsDAO.create(carCreateEntity);
                if(createResult){
                    logger.info("Car added");
                }

                break;
            case 4:
                logger.info("Enter the parameters for car you want to update");
                logger.info("Id:");
                Integer updateId = Integer.valueOf(br.readLine());
                logger.info("Brand:");
                String updateBrand = br.readLine();
                logger.info("Model:");
                String updateModel = br.readLine();
                logger.info("Type:");
                String updateType = br.readLine();
                logger.info("Max speed:");
                Integer updateMaxSpeed = Integer.valueOf(br.readLine());
                logger.info("Parameters of usage:");
                Integer updateParametersOfUsage = Integer.valueOf(br.readLine());
                CarEntity carUpdateEntity = new CarEntity(updateId, updateBrand, updateModel,
                        updateType, updateMaxSpeed, updateParametersOfUsage);
                final boolean updateResult = carsDAO.update(carUpdateEntity);
                if(updateResult){
                    logger.info("Car updated");
                }

                break;
            case 5:
                logger.info("Enter the car id which you want to delete");
                logger.info("Id:");
                Integer deleteId = Integer.valueOf(br.readLine());
                final boolean deleteResult = carsDAO.delete(String.valueOf(deleteId));
                if(deleteResult){
                    logger.info("Car deleted");
                }
                break;
        }
    }

    public void selectTableRoutesAction() throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        RoutesDAO routesDAO = new RoutesDAOImpl();
        logger.info("Table routes");
        logger.info("Select all - press 1");
        logger.info("Select cars by id - press 2");
        logger.info("Create car - press 3");
        logger.info("Update car - press 4");
        logger.info("Delete car by id - press 5");
        int choice = Integer.valueOf(br.readLine());
        switch (choice) {
            case 1:
                logger.info(routesDAO.findAll());
                break;
            case 2:
                logger.info("Enter id for search");
                int searchId = Integer.valueOf(br.readLine());
                logger.info(routesDAO.findByID(String.valueOf(searchId)));
                break;
            case 3:
                logger.info("Enter the parameters for route you want to create");
                logger.info("Id:");
                Integer createId = Integer.valueOf(br.readLine());
                logger.info("End points:");
                String createEndPoints = br.readLine();
                logger.info("Approximate time:");
                Integer createApproximateTime = Integer.valueOf(br.readLine());
                logger.info("Complexity:");
                String createComplexity = br.readLine();
                RouteEntity routeCreateEntity = new RouteEntity(createId, createEndPoints, createApproximateTime,
                        createComplexity);
                final boolean createResult = routesDAO.create(routeCreateEntity);
                if(createResult){
                    logger.info("Route added");
                }
                break;
            case 4:
                logger.info("Enter the parameters for route you want to update");
                logger.info("Id:");
                Integer updateId = Integer.valueOf(br.readLine());
                logger.info("End points:");
                String updateEndPoints = br.readLine();
                logger.info("Approximate time:");
                Integer updateApproximateTime = Integer.valueOf(br.readLine());
                logger.info("Complexity:");
                String updateComplexity = br.readLine();
                RouteEntity routeUpdateEntity = new RouteEntity(updateId, updateEndPoints, updateApproximateTime,
                        updateComplexity);
                final boolean updateResult = routesDAO.update(routeUpdateEntity);
                if(updateResult){
                    logger.info("Route updated");
                }
                break;
            case 5:
                logger.info("Enter the route id which you want to delete");
                logger.info("Id:");
                Integer deleteId = Integer.valueOf(br.readLine());
                final boolean deleteResult = routesDAO.delete(String.valueOf(deleteId));
                if(deleteResult){
                    logger.info("Route deleted");
                }
                break;
        }
    }

    public void selectTableParametersOfUsageAction() throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        ParametersOfUsageDAO parametersOfUsageDAO = new ParametersOfUsageDAOImpl();
        logger.info("Table parameters_of_usage");
        logger.info("Select all - press 1");
        logger.info("Select parameters of usage by id - press 2");
        logger.info("Create parameters of usage - press 3");
        logger.info("Update parameters of usage - press 4");
        logger.info("Delete parameters of usage by id - press 5");
        int choice = Integer.valueOf(br.readLine());
        switch (choice) {
            case 1:
                logger.info(parametersOfUsageDAO.findAll());
                break;
            case 2:
                logger.info("Enter id for search");
                int searchId = Integer.valueOf(br.readLine());
                logger.info(parametersOfUsageDAO.findByID(String.valueOf(searchId)));
                break;
            case 3:
                logger.info("Enter the parameters of usage you want to create");
                logger.info("Id:");
                Integer createId = Integer.valueOf(br.readLine());
                logger.info("Overall mileage:");
                Integer createOverallMileage = Integer.valueOf(br.readLine());
                logger.info("Cities visited:");
                Integer createCitiesVisited = Integer.valueOf(br.readLine());
                logger.info("Overall condition:");
                String createOverallCondition = br.readLine();
                ParametersOfUsageEntity parametersOfUsageCreateEntity = new ParametersOfUsageEntity(createId,
                        createOverallMileage, createCitiesVisited, createOverallCondition);
                final boolean createResult = parametersOfUsageDAO.create(parametersOfUsageCreateEntity);
                if(createResult){
                    logger.info("Parameters of usage added");
                }
                break;
            case 4:
                logger.info("Enter the parameters of usage you want to update");
                logger.info("Id:");
                Integer updateId = Integer.valueOf(br.readLine());
                logger.info("Overall mileage:");
                Integer updateOverallMileage = Integer.valueOf(br.readLine());
                logger.info("Cities visited:");
                Integer updateCitiesVisited = Integer.valueOf(br.readLine());
                logger.info("Overall condition:");
                String updateOverallCondition = br.readLine();
                ParametersOfUsageEntity parametersOfUsageUpdateEntity = new ParametersOfUsageEntity(updateId,
                        updateOverallMileage, updateCitiesVisited, updateOverallCondition);
                final boolean updateResult = parametersOfUsageDAO.create(parametersOfUsageUpdateEntity);
                if(updateResult){
                    logger.info("Parameters of usage updated");
                }
                break;
            case 5:
                logger.info("Enter the parameters of usage id which you want to delete");
                logger.info("Id:");
                Integer deleteId = Integer.valueOf(br.readLine());
                final boolean deleteResult = parametersOfUsageDAO.delete(String.valueOf(deleteId));
                if(deleteResult){
                    logger.info("Parameters of usage deleted");
                }
                break;
        }
    }

    public void selectTableCarsOnRoutesAction() throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        CarsOnRoutesDAO carsOnRoutesDAO = new CarsOnRoutesDAOImpl();
        logger.info("Table cars_on_routes");
        logger.info("Select all - press 1");
        logger.info("Select cars by route id - press 2");
        logger.info("Create car on route - press 3");
        logger.info("Update car on route - press 4");
        logger.info("Delete car on route by car and route ids - press 5");
        int choice = Integer.valueOf(br.readLine());
        switch (choice) {
            case 1:
                logger.info(carsOnRoutesDAO.findAll());
                break;
            case 2:
                logger.info("Enter route id for search");
                int searchId = Integer.valueOf(br.readLine());
                logger.info(carsOnRoutesDAO.findCarsByRouteId(String.valueOf(searchId)));
                break;
            case 3:
                logger.info("Enter the car on route parameters you want to create");
                logger.info("Car id:");
                Integer createCarId = Integer.valueOf(br.readLine());
                logger.info("Route id:");
                Integer createRouteId = Integer.valueOf(br.readLine());
                CarsOnRoutesEntity carsOnRoutesCreateEntity = new CarsOnRoutesEntity(createCarId, createRouteId);
                final boolean createResult = carsOnRoutesDAO.create(carsOnRoutesCreateEntity);
                if(createResult){
                    logger.info("Parameters of usage added");
                }
                break;
            case 4:
                logger.info("Enter the car on route parameters you want to update");
                logger.info("Car id:");
                Integer updateCarId = Integer.valueOf(br.readLine());
                logger.info("Route id:");
                Integer updateRouteId = Integer.valueOf(br.readLine());
                logger.info("Updated car id:");
                Integer updateNewCarId = Integer.valueOf(br.readLine());
                logger.info("Updated route id:");
                Integer updateNewRouteId = Integer.valueOf(br.readLine());
                CarsOnRoutesEntity carsOnRoutesUpdateEntity = new CarsOnRoutesEntity(updateNewCarId, updateNewRouteId);
                final boolean updateResult = carsOnRoutesDAO.update(carsOnRoutesUpdateEntity,
                        String.valueOf(updateCarId), String.valueOf(updateRouteId));
                if(updateResult){
                    logger.info("Car on route updated");
                }
                break;
            case 5:
                logger.info("Enter the car on route car id and route id which you want to delete");
                logger.info("Car id:");
                Integer deleteCarId = Integer.valueOf(br.readLine());
                logger.info("Route id:");
                Integer deleteRouteId = Integer.valueOf(br.readLine());
                final boolean deleteResult = carsOnRoutesDAO.delete(String.valueOf(deleteCarId), String.valueOf(deleteRouteId));
                if(deleteResult){
                    logger.info("Car on route deleted");
                }
                break;
        }
    }
}