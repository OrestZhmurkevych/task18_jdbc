package edu.jdbc.common;

public class Queries {

    public static final String FIND_ALL_CARS = "SELECT * FROM cars";
    public static final String FIND_ALL_ROUTES = "SELECT * FROM routes";
    public static final String FIND_ALL_PARAMETERS_OF_USAGE = "SELECT * FROM parameters_of_usage";
    public static final String FIND_ALL_CARS_ON_ROUTES = "SELECT * FROM cars_on_routes";
    public static final String FIND_CAR_BY_ID = "SELECT * FROM cars WHERE id =?";
    public static final String FIND_ROUTE_BY_ID = "SELECT * FROM routes WHERE id =?";
    public static final String FIND_PARAMETERS_OF_USAGE_BY_ID = "SELECT * FROM parameters_of_usage WHERE id =?";
    public static final String CREATE_CAR = "INSERT INTO cars (id, brand, model, type, max_speed, parameters_of_usage_id)"
            + " VALUES (?, ?, ?, ?, ?, ?)";
    public static final String CREATE_ROUTE = "INSERT INTO routes (id, end_points, approximate_road_time, complexity)"
            + " VALUES (?, ?, ?, ?)";
    public static final String CREATE_PARAMETERS_OF_USAGE = "INSERT INTO parameters_of_usage (id, overall_mileage,"
            + " cities_visited, overall_condition) VALUES (?, ?, ?, ?)";

    public static final String UPDATE_CAR = "UPDATE cars SET brand =?, model =?, type =?, max_speed =?,"
            + " parameters_of_usage_id =? WHERE id =?";
    public static final String UPDATE_ROUTE = "UPDATE routes SET end_points =?, approximate_road_time =?, complexity =?"
            + " WHERE id =?";
    public static final String UPDATE_PARAMETERS_OF_USAGE = "UPDATE parameters_of_usage SET overall_mileage =?,"
            + " cities_visited =?, overall_condition =? WHERE id =?";
    public static final String UPDATE_CARS_ON_ROUTES = "UPDATE cars_on_routes SET car_id =?, route_id =? "
            + "WHERE car_id =? AND route_id =?";
    public static final String DELETE_CAR = "DELETE FROM cars WHERE id =?";
    public static final String DELETE_ROUTE = "DELETE FROM routes WHERE id =?";
    public static final String DELETE_PARAMETERS_OF_USAGE = "DELETE FROM parameters_of_usage WHERE id =?";
    public static final String FIND_CARS_BY_ROUTE_ID = "SELECT car_id FROM cars_on_routes WHERE route_id =?";
    public static final String CREATE_CAR_ON_ROUTE = "INSERT INTO cars_on_routes (car_id, route_id) VALUES (?, ?)";
    public static final String DELETE_CAR_ON_ROUTE = "DELETE FROM cars_on_routes WHERE car_id =? AND route_id =?";
}