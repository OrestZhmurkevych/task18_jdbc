CREATE SCHEMA IF NOT EXISTS `transport` DEFAULT CHARACTER SET utf8 ;
USE `transport` ;

CREATE TABLE IF NOT EXISTS `transport`.`parameters_of_usage` (
  `id` INT(11) NOT NULL,
  `overall_mileage` INT(11) NULL DEFAULT NULL,
  `cities_visited` INT(11) NULL DEFAULT NULL,
  `overall_condition` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `transport`.`cars` (
  `id` INT(11) NOT NULL,
  `brand` VARCHAR(45) NULL DEFAULT NULL,
  `model` VARCHAR(45) NULL DEFAULT NULL,
  `type` VARCHAR(45) NULL DEFAULT NULL,
  `max_speed` INT(11) NULL DEFAULT NULL,
  `parameters_of_usage_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `parameters_of_usage_id_idx` (`parameters_of_usage_id` ASC),
  CONSTRAINT `parameters_of_usage_id`
    FOREIGN KEY (`parameters_of_usage_id`)
    REFERENCES `transport`.`parameters_of_usage` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `transport`.`routes` (
  `id` INT(11) NOT NULL,
  `end_points` VARCHAR(45) NOT NULL,
  `approximate_road_time` INT NOT NULL,
  `complexity` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `transport`.`cars_on_routes` (
  `car_id` INT(11) NOT NULL,
  `route_id` INT(11) NOT NULL,
  PRIMARY KEY (`car_id`, `route_id`),
  INDEX `fk_cars_has_garages_garages1_idx` (`route_id` ASC),
  INDEX `fk_cars_has_garages_cars1_idx` (`car_id` ASC),
  CONSTRAINT `fk_cars_has_garages_cars1`
    FOREIGN KEY (`car_id`)
    REFERENCES `transport`.`cars` (`id`),
  CONSTRAINT `fk_cars_has_garages_garages1`
    FOREIGN KEY (`route_id`)
    REFERENCES `transport`.`routes` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;